import React from "react";
import FullCourse from "../../components/FullCourse";
import classes from "./checkPoint.module.scss";
import { Link } from "react-router-dom";
import { AiFillLeftCircle } from "react-icons/ai";

function CheckPoint(props) {
    React.useEffect(() => {
        document.title = "Tính toán điểm tích luỹ";
    });
    const [major, setMajor] = React.useState();
    const handleChange = (e) => {
        const value = e.target.value;
        setMajor(value);
    };
    return (
        <section className={classes.root}>
            <div className={classes.selectMajor}>
                <select onChange={handleChange}>
                    <option value="">Chọn chuyên ngành</option>
                    <option value="cntt">Công nghệ thông tin</option>
                    <option value="ttdpt">Truyền thông đa phương tiện</option>
                </select>
            </div>

            {!!major && <FullCourse major={major} />}
            <div className={classes.buttonContainer}>
                <Link to={"/"} className={classes.buttonWhite}>
                    <AiFillLeftCircle />
                </Link>
            </div>
        </section>
    );
}

export default CheckPoint;
