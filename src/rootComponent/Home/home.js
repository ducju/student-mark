import React from "react";
import classes from "./home.module.scss";
import { Link } from "react-router-dom";

function Home(props) {
    return (
        <div className={classes.root}>
            <Link to={"/checkpoint"} className={classes.buttonBlue}>
                Tính điểm tích luỹ
            </Link>
        </div>
    );
}

export default Home;
